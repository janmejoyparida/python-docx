from docx import Document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_UNDERLINE
from docx.shared import Pt
from datetime import datetime
from docx.oxml.xmlchemy import OxmlElement
from docx.oxml.shared import qn


# Get the current date
current_date = datetime.now().date()

# Format the current date as "MM-DD-YYYY"
formatted_date = current_date.strftime('%m-%d-%Y')

# Create a new document
doc = Document()

# Add content to the document
doc.add_paragraph("Hello, World!")

# Access the sections in the document
sections = doc.sections

# Access the first section (by default, there should be at least one section)
section = sections[0]

# Create the header and footer objects
header = section.header
footer = section.footer

# Set the alignment for the header text
header_paragraph = header.paragraphs[0]
header_paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

# Clear existing runs in the header paragraph
for run in header_paragraph.runs:
    header_paragraph._p.remove(run._r)

# Add "Scenario 01" to the header centered
header_run = header_paragraph.add_run()
header_run.text = "Scenario 01"
header_run.bold = True
header_run.font.size = Pt(14)
header_run.font.color.rgb = None  # Reset color to default
header_run.font.underline = WD_UNDERLINE.SINGLE

# Create a new paragraph for the header text
header_text_paragraph = header.add_paragraph()

# Add "Policy Number: 17-CD-F096-1" to the header with right alignment
policy_run = header_text_paragraph.add_run()
policy_run.text = "Env: devenv5"
policy_run.bold = False
policy_run.font.size = Pt(11)
policy_run.font.color.rgb = None  # Reset color to default

# Add a tab character to create a tab stop
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()

# Add "State: KY" to the header with left alignment
state_run = header_text_paragraph.add_run()
state_run.text = "Policy Number: 17-CD-F096-1"
state_run.bold = False
state_run.font.size = Pt(11)
state_run.font.color.rgb = None  # Reset color to default

# Reduce the space after the first paragraph
header_text_paragraph.paragraph_format.space_after = Pt(1)

# Create a new paragraph for the header text
header_text_paragraph = header.add_paragraph()

# Add "Policy Number: 17-CD-F096-1" to the header with right alignment
policy_run = header_text_paragraph.add_run()
policy_run.text = "Env Link: (opt)"
policy_run.bold = False
policy_run.font.size = Pt(11)
policy_run.font.color.rgb = None  # Reset color to default

# Add a tab character to create a tab stop
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()
header_text_paragraph.add_run().add_tab()

# Add "State: KY" to the header with left alignment
state_run = header_text_paragraph.add_run()
state_run.text = "State: KY"
state_run.bold = False
state_run.font.size = Pt(11)
state_run.font.color.rgb = None  # Reset color to default

# Create a paragraph in the footer and set its alignment
footer_paragraph = footer.add_paragraph()

# Add "Policy Number: 17-CD-F096-1" to the header with right alignment
policy_run = footer_paragraph.add_run()
policy_run.text = "Executed On: " + formatted_date
policy_run.bold = False
policy_run.font.size = Pt(11)
policy_run.font.color.rgb = None  # Reset color to default

# Add a tab character to create a tab stop
footer_paragraph.add_run().add_tab()
footer_paragraph.add_run().add_tab()
footer_paragraph.add_run().add_tab()
footer_paragraph.add_run().add_tab()
footer_paragraph.add_run().add_tab()

# Add "State: KY" to the header with left alignment
state_run = footer_paragraph.add_run()
state_run.text = "Executed by: Janmejoy Parida"
state_run.bold = False
state_run.font.size = Pt(11)
state_run.font.color.rgb = None  # Reset color to default

sec_pr = doc.sections[0]._sectPr  # get the section properties el
# create new borders el
pg_borders = OxmlElement('w:pgBorders')
# specifies how the relative positioning of the borders should be calculated
pg_borders.set(qn('w:offsetFrom'), 'page')
for border_name in ('top', 'left', 'bottom', 'right',):  # set all borders
    border_el = OxmlElement(f'w:{border_name}')
    border_el.set(qn('w:val'), 'single')  # a single line
    border_el.set(qn('w:sz'), '20')  # for meaning of  remaining attrs please look docs
    border_el.set(qn('w:space'), '24')
    border_el.set(qn('w:color'), 'auto')
    pg_borders.append(border_el)  # register single border to border el
sec_pr.append(pg_borders)

# Save the document
doc.save("document.docx")
